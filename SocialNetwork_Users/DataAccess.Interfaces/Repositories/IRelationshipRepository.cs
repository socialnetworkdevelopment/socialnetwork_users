﻿using Entities;

namespace DataAccess.Interfaces.Repositories
{
    public interface IRelationshipRepository : IRepository<Relationship, long>, IRelationshipReadonlyRepository
    {
    }
}
