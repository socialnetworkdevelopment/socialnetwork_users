﻿using Entities;

namespace DataAccess.Interfaces.Repositories
{
    public interface IRelationshipRequestReadonlyRepository : IReadonlyRepository<RelationshipRequest, long>
    {
    }
}
