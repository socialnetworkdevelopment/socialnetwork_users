﻿using Entities;

namespace DataAccess.Interfaces.Repositories
{
    public interface IUserRepository : IRepository<User, long>, IUserReadonlyRepository
    {
    }
}
