﻿using Entities;

namespace DataAccess.Interfaces.Repositories
{
    public interface IUserReadonlyRepository : IReadonlyRepository<User, long>
    {
    }
}
