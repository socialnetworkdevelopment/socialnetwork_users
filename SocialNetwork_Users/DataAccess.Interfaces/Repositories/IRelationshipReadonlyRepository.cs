﻿using Entities;

namespace DataAccess.Interfaces.Repositories
{
    public interface IRelationshipReadonlyRepository : IReadonlyRepository<Relationship, long>
    {
    }
}
