﻿using Entities;

namespace DataAccess.Interfaces.Repositories
{
    public interface IRelationshipRequestRepository : IRepository<RelationshipRequest, long>, IRelationshipRequestReadonlyRepository
    {
    }
}
