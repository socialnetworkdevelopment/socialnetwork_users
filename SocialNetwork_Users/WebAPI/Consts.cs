﻿namespace WebAPI
{
    public class Consts
    {
        public const string ChatConnectionString = "PostgreChat";
        public const string IdentityServerUrlConfig = "IdentityServerUrl";
        public const string ReactClientrUrlConfig = "ReactClientUrl";
        public const string UserHubRelativePath = "/userHub";
    }
}
