﻿using Controllers.User.Dto;
using Entities.Enums;

namespace Controllers.RelationshipRequest.Dto
{
    public class IncomingRelationshipRequestDto
    {
        public int Id { get; set; }

        public required UserDto Sender { get; set; }

        public RelationshipType Type { get; set; }

        public DateTime CreateAt { get; set; }
    }
}
