﻿using AutoMapper;
using Hub.SignalR.Dto;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace Controllers.RelationshipRequest.Utils
{
    public class RelationshipRequestMapperProfile : Profile
    {
        public RelationshipRequestMapperProfile()
        {
            CreateMap<Dto.CreateRelationshipRequestDto, UseCases.Dto.CreateRelationshipRequestDto>();
            CreateMap<UseCases.Dto.IncomingRelationshipRequestDto, Dto.IncomingRelationshipRequestDto>();
            CreateMap<UseCases.Dto.IncomingRelationshipRequestsByUserDto, Dto.IncomingRelationshipRequestsByUserDto>();
            CreateMap<Entities.RelationshipRequest,Hub.SignalR.Dto.RelationshipRequestDto>()
                .ForMember(r=>r.RequestId,o=>o.MapFrom(r=>r.Id));
            CreateMap<Entities.RelationshipRequest, AcceptRelationshipRequestDto>()
                .ForMember(r => r.RequestId, o => o.MapFrom(r => r.Id))
                .ForMember(dest => dest.Receiver, opt => opt.MapFrom(src => src.Receiver));

            CreateMap<Entities.User, Hub.SignalR.Dto.UserDto>();
        }
    }

    namespace Registration
    {
        public class RelationshipRequestMapperProfileRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddAutoMapper((mapperConfiguration) =>
                {
                    mapperConfiguration.AddProfile(new RelationshipRequestMapperProfile());
                });
            }
        }
    }
}
