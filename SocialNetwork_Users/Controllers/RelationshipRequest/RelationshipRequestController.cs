﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using UseCases.Command.Accept;
using UseCases.Command.Create;
using UseCases.Command.Reject;
using UseCases.Dto;
using UseCases.Notification.Accept;
using UseCases.Notification.Reject;
using UseCases.Notification.Send;
using UseCases.Query.GetByUser;
using Utils;

namespace Controllers.RelationshipRequest
{
    [Authorize]
    [ApiController]
    [Route("api/relationshipRequest")]
    public class RelationshipRequestController : ControllerBase
    {
        private readonly IMediator _sender;
        private readonly IMapper _mapper;

        public RelationshipRequestController(IMediator sender, IMapper mapper)
        {
            _sender = sender;
            _mapper = mapper;
        }

        [HttpPost("")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(long))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Create([Required][FromBody] Dto.CreateRelationshipRequestDto dto)
        {
            CreateRelationshipRequestDto useCasesDto = _mapper.Map<CreateRelationshipRequestDto>(dto);
            CreateRelationshipRequestCommand command = new(useCasesDto);
            long requestId = await _sender.Send(command);

            SendRelationshipRequestNotification notification = new(requestId);
            await _sender.Publish(notification);

            return new ObjectResult(requestId) { StatusCode = StatusCodes.Status201Created };
        }

        [HttpGet("incomingByUser")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Dto.IncomingRelationshipRequestsByUserDto))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetIncomingByUser([Range(1, long.MaxValue)][FromQuery] long userId)
        {
            GetIncomingRelationshipRequestsByUserQuery query = new(userId);
            Maybe<IncomingRelationshipRequestsByUserDto> result = await _sender.Send(query);

            return result.ToOk(_mapper.Map<Dto.IncomingRelationshipRequestsByUserDto>);
        }

        [HttpPut("accept/{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Accept([Range(1, long.MaxValue)] long id)
        {
            AcceptRelationshipRequestCommand command = new(id);
            await _sender.Send(command);

            AcceptRelationshipRequestNotification notification = new(id);
            await _sender.Publish(notification);

            return NoContent();
        }

        [HttpPut("reject/{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Reject([Range(1, long.MaxValue)] long id)
        {
            RejectRelationshipRequestCommand command = new(id);
            await _sender.Send(command);

            RejectRelationshipRequestNotification notification = new(id);
            await _sender.Publish(notification);

            return NoContent();
        }
    }
}
