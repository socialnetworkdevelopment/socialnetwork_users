﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace Controllers.User.Utils
{
    public class UserMapperProfile : Profile
    {
        public UserMapperProfile()
        {
            CreateMap<Dto.UpdateUserDto, UseCases.Dto.UpdateUserDto>();
            CreateMap<Dto.CreateUserDto, UseCases.Dto.CreateUserDto>();
            CreateMap<Dto.UserDto, UseCases.Dto.UserDto>()
                .ReverseMap();
            CreateMap<UseCases.Dto.UserFriendsDto, Dto.UserFriendsDto>();
        }
    }

    namespace Registration
    {
        public class ChatMapperProfileRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddAutoMapper((mapperConfiguration) =>
                {
                    mapperConfiguration.AddProfile(new UserMapperProfile());
                });
            }
        }
    }
}
