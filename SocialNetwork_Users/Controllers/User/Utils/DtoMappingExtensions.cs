﻿using Controllers.User.Dto;
using SocialNetwork.Contracts.User;

namespace Controllers.User.Utils
{
    public static class DtoMappingExtensions
    {
        public static UserCreated ToMessageBusContract(this CreateUserDto dto)
        {
            return new UserCreated
            (
                UserId: dto.Id,
                FirstName: dto.FirstName,
                LastName: dto.LastName,
                AvatarId:dto.AvatarId?? -1,
                Age: dto.Age?? 0
            );
        }

        public static UserUpdated ToMessageBusContract(this UpdateUserDto dto, long userId)
        {
            return new UserUpdated
            (
                UserId: userId,
                FirstName: dto.FirstName,
                LastName: dto.LastName,
                Age: dto.Age ?? 0
            );
        }
    }
}
