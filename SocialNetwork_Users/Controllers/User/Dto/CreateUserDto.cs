﻿namespace Controllers.User.Dto
{
    public class CreateUserDto
    {
        public long Id { get; set; }

        public string? FirstName { get; set; }

        public string? LastName { get; set; }

        public long? AvatarId { get; set; }

        public int? Age { get; set; }

        public string? City { get; set; }

        public string? About { get; set; }
    }
}
