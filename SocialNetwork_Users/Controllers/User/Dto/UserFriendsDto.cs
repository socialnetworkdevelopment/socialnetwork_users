﻿namespace Controllers.User.Dto
{
    public class UserFriendsDto
    {
        public required UserDto User { get; set; }

        public IList<UserDto>? Friends { get; set; }
    }
}
