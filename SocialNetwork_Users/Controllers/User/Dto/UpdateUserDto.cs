﻿namespace Controllers.User.Dto
{
    public class UpdateUserDto
    {
        public string? FirstName { get; set; }

        public string? LastName { get; set; }

        public long? AvatarId { get; set; }

        public int? Age { get; set; }

        public string? City { get; set; }

        public string? About { get; set; }
    }
}
