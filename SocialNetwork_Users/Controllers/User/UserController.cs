﻿using AutoMapper;
using Controllers.User.Utils;
using MediatR;
using MessageBus.MassTransit.Handlers.User.Create;
using MessageBus.MassTransit.Handlers.User.Delete;
using MessageBus.MassTransit.Handlers.User.Update;
using MessageBus.MassTransit.Handlers.User.UpdateAvatar;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SocialNetwork.Contracts.User;
using System.ComponentModel.DataAnnotations;
using UseCases.Command.Create;
using UseCases.Command.Delete;
using UseCases.Command.Update;
using UseCases.Command.UpdateAvatar;
using UseCases.Dto;
using UseCases.Query.Get;
using UseCases.Query.GetFriends;
using UseCases.Query.GetRecommendationsFriends;
using UseCases.Query.SearchUsers;
using Utils;

namespace Controllers.User
{
    [ApiController]
    [Route("api/users")]
    public class UserController : ControllerBase
    {
        private readonly IMediator _sender;
        private readonly IMapper _mapper;

        public UserController(IMediator sender, IMapper mapper)
        {
            _sender = sender;
            _mapper = mapper;
        }

        [HttpPost("")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(long))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Create([Required][FromBody] Dto.CreateUserDto dto)
        {
            CreateUserCommand command = new(_mapper.Map<CreateUserDto>(dto));
            long userId = await _sender.Send(command);

            CreatedUserNotification notification = new(dto.ToMessageBusContract());
            await _sender.Publish(notification);

            return new ObjectResult(userId) { StatusCode = StatusCodes.Status201Created };
        }

        [Authorize]
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Dto.UserDto))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Get([Range(1, long.MaxValue)] long id)
        {
            GetUserQuery query = new(id);
            Maybe<UserDto> result = await _sender.Send(query);

            return result.ToOk(_mapper.Map<Dto.UserDto>);
        }

        [Authorize]
        [HttpGet("search/{searchString}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Dto.UserDto>))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SearchUsers(string searchString)
        {
            var query = new SearchUsersQuery(searchString);
            var result = await _sender.Send(query);
            return result.ToOk(_mapper.Map<IEnumerable<Dto.UserDto>>);
        }

        [Authorize]
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Delete([Range(1, long.MaxValue)] long id)
        {
            DeleteUserCommand command = new(id);
            await _sender.Send(command);

            DeletedUserNotification notification = new(new UserDeleted(id));
            await _sender.Publish(notification);

            return NoContent();
        }

        [Authorize]
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(long))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Update(long id, [Required][FromBody] Dto.UpdateUserDto userDto)
        {
            UpdateUserCommand command = new(id, _mapper.Map<UpdateUserDto>(userDto));
            Maybe<long> userId = await _sender.Send(command);

            UpdatedUserNotification notification = new(userDto.ToMessageBusContract(id));
            await _sender.Publish(notification);

            return userId.ToOk();
        }

        [Authorize]
        [HttpPatch("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(long))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateAvatar(long id, [FromBody] long avatarId)
        {
            UpdateAvatarCommand command = new(id, avatarId);
            Maybe<long> userId = await _sender.Send(command);

            UpdateUserAvatarNotification notification = new(new UserAvatarUpdated(id, avatarId));
            await _sender.Publish(notification);

            return userId.ToOk();
        }

        //[Authorize]
        [HttpGet("{id}/friends")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Dto.UserFriendsDto))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetFriends([Range(1, long.MaxValue)] long id)
        {
            GetUserFriendsQuery query = new(id);
            Maybe<UserFriendsDto> result = await _sender.Send(query);

            return result.ToOk(_mapper.Map<Dto.UserFriendsDto>);
        }

        [Authorize]
        [HttpGet("recommendations/{userId}")]
        [ProducesResponseType(StatusCodes.Status200OK,Type = typeof(IEnumerable<Dto.UserDto>))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetRecommendationsFriends([Range(1,long.MaxValue)]long userId,int count)
        {
            var query = new GetRecommendationsFriendsQuery(userId,count);
            var result = await _sender.Send(query);

            return result.ToOk(_mapper.Map<IEnumerable<Dto.UserDto>>);
        }
    }
}
