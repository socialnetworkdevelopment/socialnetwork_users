﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using UseCases.Command.Delete;

namespace Controllers.Relationship
{
    [Authorize]
    [ApiController]
    [Route("api/relationship")]
    public class RelationshopController : ControllerBase
    {
        private readonly IMediator _sender;

        public RelationshopController(IMediator mediator)
        {
            _sender = mediator;
        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Delete([Range(1,long.MaxValue)]long id1, [Range(1, long.MaxValue)] long id2)
        {
            var command = new DeleteRelationshipCommand(id1, id2);
            await _sender.Send(command);

            return NoContent();
        }
    }
}
