﻿using Utils;

namespace UseCases.Exceptions
{
    public class RelationshipRequestMissingException : ValueMissingException
    {
        public RelationshipRequestMissingException(long relationshipRequestId)
        {
            this.RelationshipRequestId = relationshipRequestId;
        }

        public long RelationshipRequestId { get; }

        protected override string CreateMessage()
        {
            return $"Failed to get the relationship request by id = {this.RelationshipRequestId}.";
        }
    }
}
