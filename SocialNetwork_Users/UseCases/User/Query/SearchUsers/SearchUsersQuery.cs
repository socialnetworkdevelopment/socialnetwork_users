﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCases.Dto;
using Utils;

namespace UseCases.Query.SearchUsers
{
    public class SearchUsersQuery : IRequest<Maybe<IEnumerable<UserDto>>>
    {
        public SearchUsersQuery(string searchString)
        {
            this.SearchString = searchString;
        }

        public string SearchString { get; set; }
    }
}
