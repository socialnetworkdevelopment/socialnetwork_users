﻿using AutoMapper;
using DataAccess.Interfaces.Repositories;
using MediatR;
using UseCases.Dto;
using Utils;

namespace UseCases.Query.SearchUsers
{
    public class SearchUsersQueryHandler : IRequestHandler<SearchUsersQuery, Maybe<IEnumerable<UserDto>>>
    {
        private readonly IMapper _mapper;
        private readonly IUserReadonlyRepository _repository;
        private readonly IRelationshipReadonlyRepository _relationshipRepository;

        public SearchUsersQueryHandler(IMapper mapper, IUserReadonlyRepository repository,
            IRelationshipReadonlyRepository relationshipRepository)
        {
            _mapper = mapper;
            _repository = repository;
            _relationshipRepository = relationshipRepository;
        }

        public async Task<Maybe<IEnumerable<UserDto>>> Handle(SearchUsersQuery request, CancellationToken cancellationToken)
        {
            var users = await _repository.GetAllAsync(s => 
            s.FirstName.ToUpper().Contains(request.SearchString.ToUpper()) ||
            s.LastName.ToUpper().Contains(request.SearchString.ToUpper()));

            var usersDto = _mapper.Map<IEnumerable<UserDto>>(users);
            return Maybe.FromValue(usersDto);
        }
    }
}
