﻿using AutoMapper;
using DataAccess.Interfaces.Repositories;
using Entities;
using MediatR;
using UseCases.Dto;
using UseCases.Exceptions;
using Utils;

namespace UseCases.Query.Get
{
    public class GetUserQueryHandler : IRequestHandler<GetUserQuery, Maybe<UserDto>>
    {
        private readonly IMapper _mapper;
        private readonly IUserReadonlyRepository _repository;

        public GetUserQueryHandler(IMapper mapper, IUserReadonlyRepository repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<Maybe<UserDto>> Handle(GetUserQuery request, CancellationToken cancellationToken)
        {
            User? user = await _repository.FirstOrDefaultAsync(user => user.Id == request.Id);
            if (user == null)
                return Maybe.FromException<UserDto>(new UserMissingException(request.Id));

            return Maybe.FromValue(_mapper.Map<UserDto>(user));
        }
    }
}
