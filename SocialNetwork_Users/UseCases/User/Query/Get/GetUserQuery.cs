﻿using MediatR;
using UseCases.Dto;
using Utils;

namespace UseCases.Query.Get
{
    public class GetUserQuery : IRequest<Maybe<UserDto>>
    {
        public GetUserQuery(long id)
        {
            this.Id = id;
        }

        public long Id { get; set; }
    }
}
