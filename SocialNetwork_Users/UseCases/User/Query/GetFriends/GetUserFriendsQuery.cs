﻿using MediatR;
using UseCases.Dto;
using Utils;

namespace UseCases.Query.GetFriends
{
    public class GetUserFriendsQuery : IRequest<Maybe<UserFriendsDto>>
    {
        public GetUserFriendsQuery(long id)
        {
            this.Id = id;
        }

        public long Id { get; set; }
    }
}
