﻿using AutoMapper;
using DataAccess.Interfaces.Repositories;
using Entities;
using MediatR;
using UseCases.Dto;
using UseCases.Exceptions;
using Utils;

namespace UseCases.Query.GetFriends
{
    public class GetUserFriendsQueryHandler : IRequestHandler<GetUserFriendsQuery, Maybe<UserFriendsDto>>
    {
        private readonly IMapper _mapper;
        private readonly IUserReadonlyRepository _repository;
        private readonly IRelationshipReadonlyRepository _relationshipRepository;

        public GetUserFriendsQueryHandler(IMapper mapper, IUserReadonlyRepository repository,
            IRelationshipReadonlyRepository relationshipRepository)
        {
            _mapper = mapper;
            _repository = repository;
            _relationshipRepository = relationshipRepository;
        }

        public async Task<Maybe<UserFriendsDto>> Handle(GetUserFriendsQuery request, CancellationToken cancellationToken)
        {
            User? user = await _repository.FirstOrDefaultAsync(user => user.Id == request.Id);

            if (user == null)
                return Maybe.FromException<UserFriendsDto>(new UserMissingException(request.Id));

            IEnumerable<Relationship> friends = await _relationshipRepository.GetAllAsync(
                relationship =>
                relationship.Type == Entities.Enums.RelationshipType.Friend
                && (relationship.User1.Id == user.Id || relationship.User2.Id == user.Id),
                includeProperties: new List<string>
                {
                    nameof(Relationship.User1),
                    nameof(Relationship.User2)
                }
                );

            List<UserDto> friendsDto = new();
            foreach (var userFriend in friends)
            {
                if (userFriend.User1.Id != user.Id)
                    friendsDto.Add(_mapper.Map<UserDto>(userFriend.User1));
                else
                    friendsDto.Add(_mapper.Map<UserDto>(userFriend.User2));
            }

            UserFriendsDto userFriends = _mapper.Map<UserFriendsDto>(user);
            userFriends.Friends = friendsDto;

            return Maybe.FromValue(userFriends);
        }
    }
}