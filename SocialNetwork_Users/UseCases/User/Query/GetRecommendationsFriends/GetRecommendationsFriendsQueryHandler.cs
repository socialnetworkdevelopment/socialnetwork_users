﻿using AutoMapper;
using DataAccess.Interfaces.Repositories;
using Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCases.Dto;
using UseCases.Query.GetFriends;
using Utils;

namespace UseCases.Query.GetRecommendationsFriends
{
    internal class GetRecommendationsFriendsQueryHandler : IRequestHandler<GetRecommendationsFriendsQuery, Maybe<List<UserDto>>>
    {
        private readonly IMapper _mapper;
        private readonly IUserReadonlyRepository _repository;
        private readonly IRelationshipReadonlyRepository _relationshipRepository;
        public GetRecommendationsFriendsQueryHandler(IMapper mapper, IUserReadonlyRepository repository,
            IRelationshipReadonlyRepository relationshipRepository)
        {
            _mapper = mapper;
            _repository = repository;
            _relationshipRepository = relationshipRepository;
        }

        public async Task<Maybe<List<UserDto>>> Handle(GetRecommendationsFriendsQuery request, CancellationToken cancellationToken)
        {
            var friends = await _relationshipRepository.GetAllAsync(r => r.User1.Id == request.UserId || r.User2.Id == request.UserId,
                includeProperties: new List<string> { nameof(Relationship.User1), nameof(Relationship.User2) });
            
            var friendsIds = friends.Select(f => f.User1.Id).ToList();
            friendsIds.AddRange(friends.Select(f => f.User2.Id));
            friendsIds = friendsIds.Distinct().ToList();

            var recommendations = await _repository.GetAllAsync(u => !friendsIds.Contains(u.Id) && u.Id != request.UserId,take:request.Count);
            
            var result = new List<UserDto>();
            foreach (var item in recommendations)
                result.Add(_mapper.Map<UserDto>(item));

            return Maybe.FromValue(result);
        }
    }
}
