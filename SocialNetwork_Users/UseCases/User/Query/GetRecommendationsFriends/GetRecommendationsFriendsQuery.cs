﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCases.Dto;
using Utils;

namespace UseCases.Query.GetRecommendationsFriends
{
    public class GetRecommendationsFriendsQuery : IRequest<Maybe<List<UserDto>>>
    {
        public GetRecommendationsFriendsQuery(long userId,int count) 
        { 
            UserId = userId;
            Count = count;
        }

        public long UserId { get; set; }
        public int Count { get; set; }
    }
}
    