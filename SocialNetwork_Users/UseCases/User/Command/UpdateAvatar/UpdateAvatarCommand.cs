﻿using MediatR;
using Utils;

namespace UseCases.Command.UpdateAvatar
{
    public class UpdateAvatarCommand : IRequest<Maybe<long>>
    {
        public UpdateAvatarCommand(long userId, long avatarId)
        {
            UserId = userId;
            AvatarId = avatarId;
        }

        public long UserId { get; }

        public long AvatarId { get; }
    }
}
