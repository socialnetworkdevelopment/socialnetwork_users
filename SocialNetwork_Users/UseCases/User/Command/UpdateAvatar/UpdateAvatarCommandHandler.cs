﻿using AutoMapper;
using DataAccess.Interfaces.Repositories;
using Entities;
using MediatR;
using UseCases.Command.Update;
using UseCases.Exceptions;
using Utils;

namespace UseCases.Command.UpdateAvatar
{
    public class UpdateAvatarCommandHandler : IRequestHandler<UpdateAvatarCommand, Maybe<long>>
    {
        private readonly IUserRepository _repository;

        public UpdateAvatarCommandHandler(IUserRepository respository)
        {
            _repository = respository;
        }

        public async Task<Maybe<long>> Handle(UpdateAvatarCommand request, CancellationToken cancellationToken)
        {
            User? existUser = await _repository.FirstOrDefaultAsync(user => user.Id == request.UserId, isTracking: false);
            if (existUser == null)
                return Maybe.FromException<long>(new UserMissingException(request.UserId));

            existUser.AvatarId = request.AvatarId;

            _repository.Update(existUser);
            await _repository.SaveAsync();
            return Maybe.FromValue(existUser.Id);
        }
    }
}
