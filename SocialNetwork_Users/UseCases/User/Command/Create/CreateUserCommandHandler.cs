﻿using AutoMapper;
using DataAccess.Interfaces.Repositories;
using MediatR;
using Entities;
using UseCases.Dto;

namespace UseCases.Command.Create
{
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, long>
    {
        private readonly IMapper _mapper;
        private readonly IUserRepository _userRepository;

        public CreateUserCommandHandler(IMapper mapper, IUserRepository userRepository)
        {
            _mapper = mapper;
            _userRepository = userRepository;
        }

        public async Task<long> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            User? existUser = await _userRepository.FindAsync(request.Dto.Id);

            if (existUser != null)
                throw new InvalidOperationException($"User with id = {request.Dto.Id} already exists.");

            User userForInsert = _mapper.Map<CreateUserDto, User>(request.Dto);

            User newUser = await _userRepository.AddAsync(userForInsert);
            await _userRepository.SaveAsync();
            
            return newUser.Id;
        }
    }
}
