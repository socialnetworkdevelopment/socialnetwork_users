﻿using MediatR;
using UseCases.Dto;

namespace UseCases.Command.Create
{
    public class CreateUserCommand : IRequest<long>
    {
        public CreateUserCommand(CreateUserDto dto)
        {
            Dto = dto ?? throw new ArgumentNullException(nameof(dto));
        }

        public CreateUserDto Dto { get; }
    }
}
