﻿using DataAccess.Interfaces.Repositories;
using Entities;
using MediatR;

namespace UseCases.Command.Delete
{
    public class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand>
    {
        private readonly IUserRepository _repository;

        public DeleteUserCommandHandler(IUserRepository respository)
        {
            _repository = respository;
        }

        public async Task Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            User? exist = await _repository.FindAsync(request.Id);
            if (exist != null)
            {
                _repository.Remove(exist);
                await _repository.SaveAsync();
            }
        }
    }
}
