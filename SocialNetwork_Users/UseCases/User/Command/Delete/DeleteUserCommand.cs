﻿using MediatR;

namespace UseCases.Command.Delete
{
    public class DeleteUserCommand : IRequest
    {
        public DeleteUserCommand(long id)
        {
            this.Id = id;
        }

        public long Id { get; }
    }
}
