﻿using AutoMapper;
using DataAccess.Interfaces.Repositories;
using Entities;
using MediatR;
using UseCases.Dto;
using UseCases.Exceptions;
using Utils;

namespace UseCases.Command.Update
{
    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, Maybe<long>>
    {
        private readonly IUserRepository _repository;
        private readonly IMapper _mapper;

        public UpdateUserCommandHandler(IUserRepository respository,
            IMapper mapper)
        {
            _repository = respository;
            _mapper = mapper;
        }

        public async Task<Maybe<long>> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            User? existUser = await _repository.FirstOrDefaultAsync(user => user.Id == request.Id, isTracking: false);
            if (existUser == null)
                return Maybe.FromException<long>(new UserMissingException(request.Id));

            _mapper.Map(request.Dto, existUser);

            _repository.Update(existUser);
            await _repository.SaveAsync();
            return Maybe.FromValue(existUser.Id);
        }
    }
}
