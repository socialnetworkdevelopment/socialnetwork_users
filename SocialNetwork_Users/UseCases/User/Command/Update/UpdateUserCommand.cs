﻿using MediatR;
using UseCases.Dto;
using Utils;

namespace UseCases.Command.Update
{
    public class UpdateUserCommand : IRequest<Maybe<long>>
    {
        public UpdateUserCommand(long id, UpdateUserDto userDto)
        {
            this.Dto = Check.NotNull(userDto);
            this.Id = id;
        }

        public UpdateUserDto Dto { get; }

        public long Id { get; }
    }
}
