﻿namespace UseCases.Dto
{
    public class UserFriendsDto
    {
        public UserDto User { get; set; }

        public IList<UserDto>? Friends { get; set; }
    }
}
