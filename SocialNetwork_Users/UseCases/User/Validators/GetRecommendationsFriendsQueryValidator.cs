﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCases.Query.SearchUsers;
using UseCases.Query.GetRecommendationsFriends;

namespace UseCases.Validators
{
    public class GetRecommendationsFriendsQueryValidator : AbstractValidator<GetRecommendationsFriendsQuery>
    {
        public GetRecommendationsFriendsQueryValidator()
        {
            RuleFor(q => q.UserId)
                .NotNull();
            RuleFor(q => q.Count)
                .NotNull();
        }
    }
}
