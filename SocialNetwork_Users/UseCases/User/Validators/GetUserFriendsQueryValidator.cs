﻿using FluentValidation;
using UseCases.Query.GetFriends;

namespace UseCases.Validators
{
    public class GetUserFriendsQueryValidator : AbstractValidator<GetUserFriendsQuery>
    {
        public GetUserFriendsQueryValidator()
        {
            RuleFor(e => e.Id)
                .GreaterThan(0);
        }
    }
}
