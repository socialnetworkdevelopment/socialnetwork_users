﻿using FluentValidation;
using UseCases.Query.SearchUsers;

namespace UseCases.Validators
{
    public class SearchUsersQueryValidator : AbstractValidator<SearchUsersQuery>
    {
        public SearchUsersQueryValidator() {
         RuleFor(q => q.SearchString)
                .NotEmpty()
                .MaximumLength(50);
        }
    }
}
