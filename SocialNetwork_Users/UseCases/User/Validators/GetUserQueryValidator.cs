﻿using FluentValidation;
using UseCases.Query.Get;

namespace UseCases.Validators
{
    public class GetUserQueryValidator : AbstractValidator<GetUserQuery>
    {
        public GetUserQueryValidator()
        {
            RuleFor(e => e.Id)
                .GreaterThan(0);
        }
    }
}
