﻿using FluentValidation;
using UseCases.Command.Update;

namespace UseCases.Validators
{
    public class UpdateUserCommandValidator : AbstractValidator<UpdateUserCommand>
    {
        public UpdateUserCommandValidator()
        {
            RuleFor(e => e.Dto)
                .NotNull();

            RuleFor(e => e.Id)
                .GreaterThan(0);

            RuleFor(e => e.Dto.FirstName)
                .MaximumLength(200);

            RuleFor(e => e.Dto.LastName)
                .MaximumLength(200);
        }
    }
}
