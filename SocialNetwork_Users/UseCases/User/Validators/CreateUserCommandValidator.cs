﻿using FluentValidation;
using UseCases.Command.Create;

namespace UseCases.Validators
{
    public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
    {
        public CreateUserCommandValidator()
        {
            RuleFor(e => e.Dto)
                .NotNull();

            RuleFor(e => e.Dto.Id)
                .GreaterThan(0);

            RuleFor(e => e.Dto.FirstName)
                .MaximumLength(200);

            RuleFor(e => e.Dto.LastName)
                .MaximumLength(200);
        }
    }
}
