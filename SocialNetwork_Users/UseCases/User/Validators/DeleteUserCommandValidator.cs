﻿using FluentValidation;
using UseCases.Command.Delete;

namespace UseCases.Validators
{
    public class DeleteUserCommandValidator : AbstractValidator<DeleteUserCommand>
    {
        public DeleteUserCommandValidator()
        {
            RuleFor(e => e.Id)
                .GreaterThan(0);
        }
    }
}
