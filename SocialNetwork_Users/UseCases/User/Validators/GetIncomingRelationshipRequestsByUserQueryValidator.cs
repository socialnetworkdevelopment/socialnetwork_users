﻿using FluentValidation;
using UseCases.Query.GetByUser;

namespace UseCases.Validators
{
    public class GetIncomingRelationshipRequestsByUserQueryValidator : AbstractValidator<GetIncomingRelationshipRequestsByUserQuery>
    {
        public GetIncomingRelationshipRequestsByUserQueryValidator()
        {
            RuleFor(e => e.UserId)
                .GreaterThan(0);
        }
    }
}
