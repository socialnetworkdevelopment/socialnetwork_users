﻿using FluentValidation;
using UseCases.Command.UpdateAvatar;

namespace UseCases.Validators
{
    public class UpdateAvatarCommandValidator : AbstractValidator<UpdateAvatarCommand>
    {
        public UpdateAvatarCommandValidator()
        {
            RuleFor(e => e.UserId)
                .GreaterThan(0);

            RuleFor(e => e.AvatarId)
                .GreaterThan(0);
        }
    }
}
