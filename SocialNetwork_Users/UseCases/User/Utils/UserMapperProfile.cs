﻿using ApplicationServices.Interfaces;
using AutoMapper;
using Entities;
using Microsoft.Extensions.DependencyInjection;
using UseCases.Dto;
using Utils;

namespace UseCases.Utils
{
    public class UserMapperProfile : Profile
    {       
        public UserMapperProfile(IDateTimeProvider dateTimeProvider)
        {
            CreateMap<CreateUserDto, User>()
                .ForMember(dest => dest.CreatedAt, opt => opt.MapFrom(src => dateTimeProvider.GetNow()));

            CreateMap<User, UserDto>();

            CreateMap<User, UserFriendsDto>()
                .ForMember(dest => dest.User, opt => opt.MapFrom(src => src))
                .ForMember(dest => dest.Friends, opt => opt.Ignore());

            CreateMap<UpdateUserDto, User>();
        }
    }

    namespace Registration
    {
        public class ChatMapperProfileRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddAutoMapper((serviceProvider, mapperConfiguration) =>
                {
                    mapperConfiguration.AddProfile(new UserMapperProfile(serviceProvider.GetRequiredService<IDateTimeProvider>()));
                },
                Array.Empty<Type>());
            }
        }
    }
}
