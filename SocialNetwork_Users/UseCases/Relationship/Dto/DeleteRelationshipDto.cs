﻿namespace UseCases.Dto
{
    public class DeleteRelationshipDto
    {
        public long Id1 { get; set; }
        public long Id2 { get; set; }
    }
}
