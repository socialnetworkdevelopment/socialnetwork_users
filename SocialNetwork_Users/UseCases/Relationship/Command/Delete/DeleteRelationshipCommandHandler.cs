﻿using DataAccess.Interfaces.Repositories;
using MediatR;

namespace UseCases.Command.Delete
{
    public class DeleteRelationshipCommandHandler : IRequestHandler<DeleteRelationshipCommand>
    {
        private readonly IRelationshipRepository _relationshipRepository;

        public DeleteRelationshipCommandHandler(IRelationshipRepository relationshipRepository)
        {
            _relationshipRepository = relationshipRepository;
        }

        public async Task Handle(DeleteRelationshipCommand request,CancellationToken cancellationToken)
        {
            var relationshipTarget = await _relationshipRepository.FirstOrDefaultAsync(r => 
            (r.User1.Id == request.Dto.Id1 && r.User2.Id == request.Dto.Id2) ||
            (r.User1.Id == request.Dto.Id2 && r.User2.Id == request.Dto.Id1));

            _relationshipRepository.Remove(relationshipTarget);
            
            await _relationshipRepository.SaveAsync();
        }
    }
}
