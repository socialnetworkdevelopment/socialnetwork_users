﻿using MediatR;
using UseCases.Dto;

namespace UseCases.Command.Delete
{
    public class DeleteRelationshipCommand : IRequest
    {
        public DeleteRelationshipCommand(long id1,long id2) {
            Dto = new DeleteRelationshipDto() { Id1 = id1, Id2 = id2 };
        }

        public DeleteRelationshipDto Dto { get; set; }
    }
}
