﻿using FluentValidation;
using UseCases.Command.Delete;

namespace UseCases.Validators
{
    public class DeleteRelationshipCommandValidator : AbstractValidator<DeleteRelationshipCommand>
    {
        public DeleteRelationshipCommandValidator() {
            RuleFor(s => s.Dto)
                .NotNull();

            RuleFor(s => s.Dto.Id1)
                .GreaterThan(0);

            RuleFor(s => s.Dto.Id2)
                .GreaterThan(0);
        }
    }
}
