﻿using Entities.Enums;

namespace UseCases.Dto
{
    public class IncomingRelationshipRequestDto
    {
        public int Id { get; set; }

        public required UserDto Sender { get; set; }

        public RelationshipType Type { get; set; }

        public DateTime CreateAt { get; set; }
    }
}
