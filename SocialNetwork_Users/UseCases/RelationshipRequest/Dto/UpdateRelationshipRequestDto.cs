﻿using Entities.Enums;

namespace UseCases.Dto
{
    public class UpdateRelationshipRequestDto
    {
        public long RelationshipRequestId { get; set; }

        public long Acce { get; set; }

        public RelationshipType Type { get; set; }
    }
}
