﻿
namespace UseCases.Dto
{
    public class IncomingRelationshipRequestsByUserDto
    {
        public required UserDto User { get; set; }

        public IList<IncomingRelationshipRequestDto>? RelationshipRequests { get; set; }
    }
}
