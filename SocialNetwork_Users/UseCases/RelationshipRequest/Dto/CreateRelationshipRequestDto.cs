﻿using Entities.Enums;

namespace UseCases.Dto
{
    public class CreateRelationshipRequestDto
    {
        public long SenderId { get; set; }

        public long ReceiverId { get; set; }

        public RelationshipType Type { get; set; }
    }
}
