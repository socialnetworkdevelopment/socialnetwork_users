﻿using FluentValidation;
using UseCases.Command.Accept;

namespace UseCases.Validators
{
    public class AcceptRelationshipRequestCommandValidator : AbstractValidator<AcceptRelationshipRequestCommand>
    {
        public AcceptRelationshipRequestCommandValidator()
        {
            RuleFor(e => e.RelatonshipRequestId)
                .GreaterThan(0);
        }
    }
}
