﻿using FluentValidation;
using UseCases.Command.Create;

namespace UseCases.Validators
{
    public class CreateRelationshipRequestCommandValidator : AbstractValidator<CreateRelationshipRequestCommand>
    {
        public CreateRelationshipRequestCommandValidator()
        {
            RuleFor(e => e.Dto)
                .NotNull();

            RuleFor(e => e.Dto.SenderId)
                .GreaterThan(0);

            RuleFor(e => e.Dto.ReceiverId)
                .GreaterThan(0);
        }
    }
}
