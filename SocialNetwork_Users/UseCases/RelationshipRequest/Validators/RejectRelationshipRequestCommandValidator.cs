﻿using FluentValidation;
using UseCases.Command.Reject;

namespace UseCases.Validators
{
    public class RejectRelationshipRequestCommandValidator : AbstractValidator<RejectRelationshipRequestCommand>
    {
        public RejectRelationshipRequestCommandValidator()
        {
            RuleFor(e => e.RelatonshipRequestId)
                .GreaterThan(0);
        }
    }
}
