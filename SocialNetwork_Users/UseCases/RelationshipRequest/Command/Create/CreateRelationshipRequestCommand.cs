﻿using MediatR;
using UseCases.Dto;

namespace UseCases.Command.Create
{
    public class CreateRelationshipRequestCommand : IRequest<long>
    {
        public CreateRelationshipRequestCommand(CreateRelationshipRequestDto dto)
        {
            Dto = dto ?? throw new ArgumentNullException(nameof(dto));
        }

        public CreateRelationshipRequestDto Dto { get; }
    }
}
