﻿using ApplicationServices.Interfaces;
using AutoMapper;
using DataAccess.Interfaces.Repositories;
using Entities;
using MediatR;
using UseCases.Exceptions;

namespace UseCases.Command.Create
{
    public class CreateRelationshipRequestCommandHandler : IRequestHandler<CreateRelationshipRequestCommand, long>
    {
        private readonly IMapper _mapper;
        private readonly IRelationshipRequestRepository _relationshipRequestRepository;
        private readonly IUserReadonlyRepository _userRepository;
        private readonly IDateTimeProvider _dateTimeProvider;

        public CreateRelationshipRequestCommandHandler(IMapper mapper, 
            IRelationshipRequestRepository relationshipRequestRepository,
            IUserReadonlyRepository userRepository,
            IDateTimeProvider dateTimeProvider)
        {
            _mapper = mapper;
            _relationshipRequestRepository = relationshipRequestRepository;
            _userRepository = userRepository;
            _dateTimeProvider = dateTimeProvider;
        }

        public async Task<long> Handle(CreateRelationshipRequestCommand request, CancellationToken cancellationToken)
        {
            RelationshipRequest? existRelationshipRequest = await _relationshipRequestRepository.FirstOrDefaultAsync(relationshipRequest => 
                relationshipRequest.Sender.Id == request.Dto.SenderId 
                && relationshipRequest.Receiver.Id == request.Dto.ReceiverId
                && relationshipRequest.Type == request.Dto.Type
                && !(relationshipRequest.Rejected || relationshipRequest.Accepted));

            if (existRelationshipRequest != null)
                throw new InvalidOperationException($"Processed relationship request with SenderId = {request.Dto.SenderId} " +
                    $"and RecieverId = {request.Dto.ReceiverId} is already exists.");

            User senderUser = await _userRepository.FindAsync(request.Dto.SenderId)
                ?? throw new UserMissingException(request.Dto.SenderId);
            User receiverUser = await _userRepository.FindAsync(request.Dto.ReceiverId)
                ?? throw new UserMissingException(request.Dto.ReceiverId);

            RelationshipRequest relationshipRequest = new()
            {
                Sender = senderUser,
                Receiver = receiverUser,
                Type = request.Dto.Type,
                CreateAt = _dateTimeProvider.GetNow()
            };

            RelationshipRequest newRelationshipRequest = await _relationshipRequestRepository.AddAsync(relationshipRequest);
            await _relationshipRequestRepository.SaveAsync();

            return newRelationshipRequest.Id;
        }
    }
}
