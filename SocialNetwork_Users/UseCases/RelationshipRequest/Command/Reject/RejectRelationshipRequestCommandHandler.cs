﻿using DataAccess.Interfaces.Repositories;
using Entities;
using MediatR;
using UseCases.Exceptions;

namespace UseCases.Command.Reject
{
    public class RejectRelationshipRequestCommandHandler : IRequestHandler<RejectRelationshipRequestCommand>
    {
        private readonly IRelationshipRequestRepository _relationshipRequestRepository;

        public RejectRelationshipRequestCommandHandler(IRelationshipRequestRepository relationshipRequestRepository)
        {
            _relationshipRequestRepository = relationshipRequestRepository;
        }

        public async Task Handle(RejectRelationshipRequestCommand request, CancellationToken cancellationToken)
        {
            RelationshipRequest? relationshipRequest = await _relationshipRequestRepository.FindAsync(request.RelatonshipRequestId);

            if (relationshipRequest == null)
                throw new RelationshipRequestMissingException(request.RelatonshipRequestId);

            if (relationshipRequest.Accepted)
                throw new InvalidOperationException($"Relationship request with id {request.RelatonshipRequestId} is already accepted.");

            if (relationshipRequest.Rejected)
                throw new InvalidOperationException($"Relationship request with id {request.RelatonshipRequestId} is already rejected.");

            relationshipRequest.Rejected = true;
            _relationshipRequestRepository.Update(relationshipRequest);
            await _relationshipRequestRepository.SaveAsync();
        }
    }
}
