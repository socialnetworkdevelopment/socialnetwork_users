﻿using MediatR;

namespace UseCases.Command.Reject
{
    public class RejectRelationshipRequestCommand : IRequest
    {
        public RejectRelationshipRequestCommand(long relatonshipRequestId)
        {
            RelatonshipRequestId = relatonshipRequestId;
        }

        public long RelatonshipRequestId { get; }
    }
}
