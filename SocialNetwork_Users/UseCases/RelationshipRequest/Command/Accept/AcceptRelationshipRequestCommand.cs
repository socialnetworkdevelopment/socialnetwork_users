﻿using MediatR;

namespace UseCases.Command.Accept
{
    public class AcceptRelationshipRequestCommand : IRequest
    {
        public AcceptRelationshipRequestCommand(long relatonshipRequestId)
        {
            RelatonshipRequestId = relatonshipRequestId;
        }

        public long RelatonshipRequestId { get; }
    }
}
