﻿using DataAccess.Interfaces.Repositories;
using Entities;
using MediatR;
using UseCases.Exceptions;

namespace UseCases.Command.Accept
{
    public class AcceptRelationshipRequestCommandHandler : IRequestHandler<AcceptRelationshipRequestCommand>
    {
        private readonly IRelationshipRequestRepository _relationshipRequestRepository;
        private readonly IRelationshipRepository _relationshipRepository;

        public AcceptRelationshipRequestCommandHandler(IRelationshipRequestRepository relationshipRequestRepository,
            IRelationshipRepository relationshipRepository)
        {
            _relationshipRequestRepository = relationshipRequestRepository;
            _relationshipRepository = relationshipRepository;
        }

        public async Task Handle(AcceptRelationshipRequestCommand request, CancellationToken cancellationToken)
        {           
            RelationshipRequest? relationshipRequest = await _relationshipRequestRepository
                .FirstOrDefaultAsync(relationshipRequest => relationshipRequest.Id == request.RelatonshipRequestId,
                includeProperties: new List<string>
                {
                    nameof(RelationshipRequest.Sender),
                    nameof(RelationshipRequest.Receiver)
                });

            if (relationshipRequest == null)
                throw new RelationshipRequestMissingException(request.RelatonshipRequestId);

            if (relationshipRequest.Accepted)
                throw new InvalidOperationException($"Relationship request with id {request.RelatonshipRequestId} is already accepted.");

            if (relationshipRequest.Rejected)
                throw new InvalidOperationException($"Relationship request with id {request.RelatonshipRequestId} is already rejected.");

            relationshipRequest.Accepted = true;

            _relationshipRequestRepository.Update(relationshipRequest);

            var oldRelationship = await _relationshipRepository.FirstOrDefaultAsync(s =>
            (s.User1 == relationshipRequest.Sender && s.User2 == relationshipRequest.Receiver) ||
            (s.User1 == relationshipRequest.Receiver && s.User2 == relationshipRequest.Sender));

            if(oldRelationship == null) 
            {
                Relationship relationship = new()
                {
                    User1 = relationshipRequest.Sender,
                    User2 = relationshipRequest.Receiver,
                    Type = relationshipRequest.Type,
                };
                await _relationshipRepository.AddAsync(relationship);
            }
            else
            {
                oldRelationship.Type = relationshipRequest.Type;
                _relationshipRepository.Update(oldRelationship);
            }

            await _relationshipRequestRepository.SaveAsync();
        }
    }
}
