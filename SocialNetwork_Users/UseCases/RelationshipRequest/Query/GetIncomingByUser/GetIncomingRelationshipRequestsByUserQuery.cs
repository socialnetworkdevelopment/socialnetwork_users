﻿using MediatR;
using UseCases.Dto;
using Utils;

namespace UseCases.Query.GetByUser
{
    public class GetIncomingRelationshipRequestsByUserQuery : IRequest<Maybe<IncomingRelationshipRequestsByUserDto>>
    {
        public GetIncomingRelationshipRequestsByUserQuery(long userId)
        {
            this.UserId = userId;
        }

        public long UserId { get; set; }
    }
}
