﻿using AutoMapper;
using DataAccess.Interfaces.Repositories;
using Entities;
using MediatR;
using UseCases.Dto;
using UseCases.Exceptions;
using Utils;

namespace UseCases.Query.GetByUser
{
    public class GetIncomingRelationshipRequestsByUserQueryHandler : IRequestHandler<GetIncomingRelationshipRequestsByUserQuery, Maybe<IncomingRelationshipRequestsByUserDto>>
    {
        private readonly IMapper _mapper;
        private readonly IUserReadonlyRepository _repository;
        private readonly IRelationshipRequestReadonlyRepository _relationshipRequestRepository;

        public GetIncomingRelationshipRequestsByUserQueryHandler(IMapper mapper, IUserReadonlyRepository repository,
            IRelationshipRequestReadonlyRepository relationshipRequestRepository)
        {
            _mapper = mapper;
            _repository = repository;
            _relationshipRequestRepository = relationshipRequestRepository;
        }

        public async Task<Maybe<IncomingRelationshipRequestsByUserDto>> Handle(GetIncomingRelationshipRequestsByUserQuery request, CancellationToken cancellationToken)
        {
            User? user = await _repository.FirstOrDefaultAsync(user => user.Id == request.UserId);

            if (user == null)
                return Maybe.FromException<IncomingRelationshipRequestsByUserDto>(new UserMissingException(request.UserId));

            IEnumerable<RelationshipRequest> relationshipRequests = await _relationshipRequestRepository.GetAllAsync(relationshipRequest =>
                relationshipRequest.Receiver.Id == request.UserId && !relationshipRequest.Accepted && !relationshipRequest.Rejected,
                includeProperties: new List<string>
                {
                    nameof(RelationshipRequest.Sender)
                });

            List<IncomingRelationshipRequestDto> incomingRelationshipRequestsDto = new();
            foreach (var relationshipRequest in relationshipRequests)
            {
                incomingRelationshipRequestsDto.Add(_mapper.Map<IncomingRelationshipRequestDto>(relationshipRequest));
            }

            IncomingRelationshipRequestsByUserDto relationshipRequestsByUser = new()
            { 
                User = _mapper.Map<UserDto>(user),
            };
            relationshipRequestsByUser.RelationshipRequests = incomingRelationshipRequestsDto;

            return Maybe.FromValue(relationshipRequestsByUser);
        }
    }
}
