﻿using ApplicationServices.Interfaces;
using AutoMapper;
using Entities;
using Microsoft.Extensions.DependencyInjection;
using UseCases.Dto;
using Utils;

namespace UseCases.Utils
{
    public class RelationshipRequestMapperProfile : Profile
    {       
        public RelationshipRequestMapperProfile(IDateTimeProvider dateTimeProvider)
        {
            CreateMap<RelationshipRequest, IncomingRelationshipRequestDto>();
        }
    }

    namespace Registration
    {
        public class RelationshipRequestMapperProfileRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddAutoMapper((serviceProvider, mapperConfiguration) =>
                {
                    mapperConfiguration.AddProfile(new RelationshipRequestMapperProfile(serviceProvider.GetRequiredService<IDateTimeProvider>()));
                },
                Array.Empty<Type>());
            }
        }
    }
}
