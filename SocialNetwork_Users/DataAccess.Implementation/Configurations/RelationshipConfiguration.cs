﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Entities;

namespace DataAccess.Implementation.Configurations
{
    internal class RelationshipConfiguration : IEntityTypeConfiguration<Relationship>
    {
        public void Configure(EntityTypeBuilder<Relationship> builder)
        {
            builder.HasKey(relationship => relationship.Id);
            builder.HasOne(relationship => relationship.User1)
                .WithMany(user => user.Relationships1)
                .IsRequired();

            builder.HasOne(relationship => relationship.User2)
                .WithMany(user => user.Relationships2)
                .IsRequired();
        }
    }
}
