﻿using Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Implementation.Configurations
{
    internal class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(user => user.Id);

            // Test data.
            builder.HasData(
                new User
                {
                    Id = 1,
                    FirstName = "John",
                    LastName = "Smith",
                    Age = 30,
                },
                new User
                {
                    Id = 2,
                    FirstName = "Miles",
                    LastName = "Morales",
                    Age = 25,
                },
                new User
                {
                    Id = 3,
                    FirstName = "Petr",
                    LastName = "Ivanov",
                    Age = 54
                }
            );
        }
    }
}
