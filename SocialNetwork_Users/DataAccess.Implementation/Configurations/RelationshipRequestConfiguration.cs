﻿using Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Implementation.Configurations
{
    internal class RelationshipRequestConfiguration : IEntityTypeConfiguration<RelationshipRequest>
    {
        public void Configure(EntityTypeBuilder<RelationshipRequest> builder)
        {
            builder.HasKey(relationshipRequest => relationshipRequest.Id);
            builder.HasOne(relationshipRequest => relationshipRequest.Sender)
                .WithMany(user => user.OutgoingRelationshipRequests)
                .IsRequired();
            builder.HasOne(relationshipRequest => relationshipRequest.Receiver)
                .WithMany(user => user.IncomingRelationshipRequests)
                .IsRequired();
        }
    }
}
