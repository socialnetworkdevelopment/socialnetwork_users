﻿using DataAccess.Interfaces;
using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq.Expressions;
using Utils;

namespace DataAccess.Implementation
{
    public class ReadonlyRepository<TEntity, TKey> : IReadonlyRepository<TEntity, TKey>
        where TEntity : BaseEntity<TKey>
    {
        protected readonly AppDbContext _db;
        internal DbSet<TEntity> dbSet;

        public ReadonlyRepository(AppDbContext db)
        {
            _db = db ?? throw new ArgumentNullException(nameof(db));
            dbSet = _db.Set<TEntity>();
        }

        public async Task<TEntity?> FindAsync(TKey id)
        {
            return await dbSet.FindAsync(id);
        }

        public async Task<TEntity?> FirstOrDefaultAsync(Expression<Func<TEntity, bool>>? filter = null, List<string>? includeProperties = null, bool isTracking = true)
        {
            IQueryable<TEntity> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeProperties != null)
            {
                foreach (string includeProperty in includeProperties)
                {
                    query = query.Include(includeProperty);
                }
            }

            if (!isTracking)
            {
                query = query.AsNoTracking();
            }

            return await query.FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>>? filter = null, 
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null, List<string>? includeProperties = null,
            int? take = null,
            int? skip = null,
            bool descending = false,
            bool isTracking = true)
        {
            IQueryable<TEntity> query = dbSet;
            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeProperties != null)
            {
                foreach (string includeProperty in includeProperties)
                {
                    query = query.Include(includeProperty);
                }
            }

            if (!isTracking)
            {
                query = query.AsNoTracking();
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (skip != null && skip > 0)
            {
                query = query.Skip(skip.Value);
            }

            if (take != null && take > 0)
            {
                query = query.Take(take.Value);
            }

            return await query.ToListAsync();
        }
    }

    namespace Registration
    {
        public class ReadonlyRepositoryRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddScoped(typeof(IReadonlyRepository<,>), typeof(ReadonlyRepository<,>));
            }
        }
    }
}
