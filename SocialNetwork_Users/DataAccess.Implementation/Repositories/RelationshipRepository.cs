﻿using DataAccess.Interfaces.Repositories;
using Entities;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace DataAccess.Implementation.Repositories
{
    public class RelationshipRepository : Repository<Relationship, long>, IRelationshipRepository
    {
        public RelationshipRepository(AppDbContext db) : base(db)
        {
        }
    }

    namespace Registration
    {
        public class RelationshipRepositoryRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddScoped<IRelationshipRepository, RelationshipRepository>();
                Check.NotNull(services).AddScoped<IRelationshipReadonlyRepository, RelationshipRepository>();
            }
        }
    }
}
