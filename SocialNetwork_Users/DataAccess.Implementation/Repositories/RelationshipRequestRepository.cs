﻿using DataAccess.Interfaces.Repositories;
using Entities;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace DataAccess.Implementation.Repositories
{
    public class RelationshipRequestRepository : Repository<RelationshipRequest, long>, IRelationshipRequestRepository
    {
        public RelationshipRequestRepository(AppDbContext db) : base(db)
        {
        }
    }

    namespace Registration
    {
        public class RelationshipRequestRepositoryRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddScoped<IRelationshipRequestRepository, RelationshipRequestRepository>();
                Check.NotNull(services).AddScoped<IRelationshipRequestReadonlyRepository, RelationshipRequestRepository>();
            }
        }
    }
}
