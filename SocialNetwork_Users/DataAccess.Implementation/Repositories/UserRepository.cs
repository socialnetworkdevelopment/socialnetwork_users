﻿using DataAccess.Interfaces.Repositories;
using Entities;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace DataAccess.Implementation.Repositories
{
    public class UserRepository : Repository<User, long>, IUserRepository
    {
        public UserRepository(AppDbContext db) : base(db)
        {
        }
    }

    namespace Registration
    {
        public class UserRepositoryRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddScoped<IUserRepository, UserRepository>();
                Check.NotNull(services).AddScoped<IUserReadonlyRepository, UserRepository>();
            }
        }
    }
}
