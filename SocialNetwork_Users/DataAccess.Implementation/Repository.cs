﻿using DataAccess.Interfaces;
using Entities;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace DataAccess.Implementation
{
    public class Repository<TEntity, TKey> : ReadonlyRepository<TEntity, TKey>, IRepository<TEntity, TKey>
        where TEntity : BaseEntity<TKey>
    {
        public Repository(AppDbContext db) : base(db) { }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            EntityEntry<TEntity> result = await dbSet.AddAsync(entity);
            return result.Entity;
        }

        public void Remove(TEntity entity)
        {
            dbSet.Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            dbSet.RemoveRange(entities);
        }

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }

        public TEntity Update(TEntity entity)
        {
            EntityEntry<TEntity> result = _db.Update(entity);
            return result.Entity;
        }
    }
}
