﻿using Hub.SignalR.Dto;

namespace Hub.SignalR.Interfaces
{
    public interface IUserHub
    {
        Task ReceiveRelationshipRequest(RelationshipRequestDto relationshipRequest);

        Task AcceptRelationshipRequest(AcceptRelationshipRequestDto relationshipRequest);

        Task RejectRelationshipRequest(RelationshipRequestDto relationshipRequest);
    }
}
