﻿namespace Hub.SignalR.Interfaces
{
    public interface IConnectionsMappingService
    {
        int Count { get; }

        void Add(long key, string connectionId);

        IEnumerable<string> GetConnections(long key);

        void Remove(long key, string connectionId);
    }
}
