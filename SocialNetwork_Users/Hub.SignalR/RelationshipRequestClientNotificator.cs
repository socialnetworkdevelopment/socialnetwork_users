﻿using AutoMapper;
using ChatNotification.Interfaces;
using Entities;
using Hub.SignalR.Dto;
using Hub.SignalR.Implementation;
using Hub.SignalR.Interfaces;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace Hub.SignalR
{
    public class RelationshipRequestClientNotificator : IRelationshipRequestClientNotificator
    {
        private readonly IHubContext<UserHub, IUserHub> _userHub;
        private readonly IMapper _mapper;

        public RelationshipRequestClientNotificator(IHubContext<UserHub, IUserHub> userHub, IMapper mapper)
        {
            _userHub = userHub;
            _mapper = mapper;
        }

        public async Task SendRelationshipRequest(RelationshipRequest relationshipRequest)
        {
            ArgumentNullException.ThrowIfNull(relationshipRequest);

            var dto = _mapper.Map<RelationshipRequestDto>(relationshipRequest);

            await _userHub.Clients.User(relationshipRequest.Receiver.Id.ToString())
                .ReceiveRelationshipRequest(dto);
        }

        public async Task AcceptRelationshipRequest(RelationshipRequest relationshipRequest)
        {
            ArgumentNullException.ThrowIfNull(relationshipRequest);

            var dto = _mapper.Map<AcceptRelationshipRequestDto>(relationshipRequest);

            await _userHub.Clients.User(relationshipRequest.Sender.Id.ToString())
                .AcceptRelationshipRequest(dto);
        }

        public async Task RejectRelationshipRequest(RelationshipRequest relationshipRequest)
        {
            ArgumentNullException.ThrowIfNull(relationshipRequest);

            var dto = _mapper.Map<RelationshipRequestDto>(relationshipRequest);

            await _userHub.Clients.User(relationshipRequest.Receiver.Id.ToString())
                .RejectRelationshipRequest(dto);
        }
    }

    namespace Registration
    {
        public class ChatClientNotificatorRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services)
                    .AddScoped<IRelationshipRequestClientNotificator, RelationshipRequestClientNotificator>();
            }
        }
    }
}
