﻿using Hub.SignalR.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace Hub.SignalR.Implementation
{
    [Authorize]
    public class UserHub : Hub<IUserHub>
    {
        public UserHub()
        {
        }
    }

    namespace Registration
    {
        public class ChatHubRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services)
                    .AddSignalR(options =>
                    {
                        options.EnableDetailedErrors = true;
                    });
            }
        }
    }
}