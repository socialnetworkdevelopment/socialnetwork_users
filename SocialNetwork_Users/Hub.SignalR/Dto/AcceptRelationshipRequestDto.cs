﻿namespace Hub.SignalR.Dto
{
    public class AcceptRelationshipRequestDto
    {
        public long RequestId { get; set; }

        public required string Type { get; set; }

        public UserDto Receiver { get; set; }
    }
}
