﻿namespace Hub.SignalR.Dto
{
    public class UserDto
    {
        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public long? AvatarId { get; set; }
    }
}
