﻿namespace Hub.SignalR.Dto
{
    public class RelationshipRequestDto
    {
        public long RequestId { get; set; }

        public required string Type { get; set; }

        public UserDto Sender { get; set; }
    }
}
