﻿using AutoMapper;
using Entities;
using Hub.SignalR.Dto;

namespace Hub.SignalR.Utils
{
    internal class RelationshipRequestMapperProfile : Profile
    {
        public RelationshipRequestMapperProfile()
        {
            CreateMap<RelationshipRequest, RelationshipRequestDto>()
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.Type.ToString()))
                .ForMember(dest => dest.RequestId, opt => opt.MapFrom(src => src.Id));
            CreateMap<User, UserDto>();
        }
    }
}
