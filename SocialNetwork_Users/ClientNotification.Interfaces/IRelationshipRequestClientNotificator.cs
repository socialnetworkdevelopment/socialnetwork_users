﻿using Entities;

namespace ChatNotification.Interfaces
{
    public interface IRelationshipRequestClientNotificator
    {
        Task SendRelationshipRequest(RelationshipRequest relationshipRequest);

        Task AcceptRelationshipRequest(RelationshipRequest relationshipRequest);

        Task RejectRelationshipRequest(RelationshipRequest relationshipRequest);
    }
}