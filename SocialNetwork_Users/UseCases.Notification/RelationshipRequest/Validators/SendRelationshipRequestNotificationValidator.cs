﻿using FluentValidation;
using UseCases.Notification.Send;

namespace UseCases.Notification.Validators
{
    public class SendRelationshipRequestNotificationValidator : AbstractValidator<SendRelationshipRequestNotification>
    {
        public SendRelationshipRequestNotificationValidator()
        {
            RuleFor(e => e.RequestId)
                .GreaterThan(0);
        }
    }
}
