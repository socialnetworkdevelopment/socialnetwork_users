﻿using FluentValidation;
using UseCases.Notification.Accept;

namespace UseCases.Notification.Validators
{
    public class AcceptRelationshipRequestNotificationValidator : AbstractValidator<AcceptRelationshipRequestNotification>
    {
        public AcceptRelationshipRequestNotificationValidator()
        {
            RuleFor(e => e.RequestId)
                .GreaterThan(0);
        }
    }
}
