﻿using FluentValidation;
using UseCases.Notification.Accept;

namespace UseCases.Notification.Validators
{
    public class RejectRelationshipRequestNotificationValidator : AbstractValidator<AcceptRelationshipRequestNotification>
    {
        public RejectRelationshipRequestNotificationValidator()
        {
            RuleFor(e => e.RequestId)
                .GreaterThan(0);
        }
    }
}
