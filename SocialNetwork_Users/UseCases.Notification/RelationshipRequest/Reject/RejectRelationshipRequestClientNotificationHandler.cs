﻿using ChatNotification.Interfaces;
using DataAccess.Interfaces.Repositories;
using Entities;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System;
using UseCases.Exceptions;
using UseCases.Validation;
using Utils;

namespace UseCases.Notification.Reject
{
    public class RejectRelationshipRequestClientNotificationHandler : INotificationHandler<RejectRelationshipRequestNotification>
    {
        private readonly IServiceProvider _serviceProvider;

        public RejectRelationshipRequestClientNotificationHandler(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task Handle(RejectRelationshipRequestNotification notification, CancellationToken cancellationToken)
        {
            using (var scope = _serviceProvider.CreateAsyncScope())
            {
                IRelationshipRequestReadonlyRepository requestReadonlyRepository = scope.ServiceProvider.GetRequiredService<IRelationshipRequestReadonlyRepository>();
                RelationshipRequest relationshipRequest = await requestReadonlyRepository
                .FirstOrDefaultAsync(request => request.Id == notification.RequestId,
                includeProperties: new List<string> { nameof(RelationshipRequest.Sender), nameof(RelationshipRequest.Receiver) })
                ?? throw new RelationshipRequestMissingException(notification.RequestId);

                await scope.ServiceProvider.GetRequiredService<IRelationshipRequestClientNotificator>()
                    .RejectRelationshipRequest(relationshipRequest);
            }
        }
    }

    namespace Registration
    {
        public class CreatedUserNotificationHandlerRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                services.AddSingleton<INotificationHandler<RejectRelationshipRequestNotification>, RejectRelationshipRequestClientNotificationHandler>()
                    .Decorate<INotificationHandler<RejectRelationshipRequestNotification>, BaseValidatorNotificationHandler<RejectRelationshipRequestNotification>>();
            }
        }
    }
}
