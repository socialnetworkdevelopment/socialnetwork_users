﻿using MediatR;

namespace UseCases.Notification.Reject
{
    public class RejectRelationshipRequestNotification : INotification
    {
        public RejectRelationshipRequestNotification(long requestId)
        {
            RequestId = requestId;
        }

        public long RequestId { get; set; }
    }
}
