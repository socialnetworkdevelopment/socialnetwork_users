﻿using ChatNotification.Interfaces;
using DataAccess.Interfaces.Repositories;
using Entities;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using UseCases.Exceptions;
using UseCases.Validation;
using Utils;

namespace UseCases.Notification.Accept
{
    public class AcceptRelationshipRequestClientNotificationHandler : INotificationHandler<AcceptRelationshipRequestNotification>
    {
        private readonly IServiceProvider _serviceProvider;

        public AcceptRelationshipRequestClientNotificationHandler(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task Handle(AcceptRelationshipRequestNotification notification, CancellationToken cancellationToken)
        {
            using (var scope = _serviceProvider.CreateAsyncScope())
            {
                IRelationshipRequestReadonlyRepository requestReadonlyRepository = scope.ServiceProvider.GetRequiredService<IRelationshipRequestReadonlyRepository>();
                RelationshipRequest relationshipRequest = await requestReadonlyRepository
                    .FirstOrDefaultAsync(request => request.Id == notification.RequestId,
                    includeProperties: new List<string> { nameof(RelationshipRequest.Sender),nameof(RelationshipRequest.Receiver) })
                    ?? throw new RelationshipRequestMissingException(notification.RequestId);

                await scope.ServiceProvider.GetRequiredService<IRelationshipRequestClientNotificator>()
                    .AcceptRelationshipRequest(relationshipRequest);
            }
        }
    }

    namespace Registration
    {
        public class CreatedUserNotificationHandlerRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                services.AddSingleton<INotificationHandler<AcceptRelationshipRequestNotification>, AcceptRelationshipRequestClientNotificationHandler>()
                    .Decorate<INotificationHandler<AcceptRelationshipRequestNotification>, BaseValidatorNotificationHandler<AcceptRelationshipRequestNotification>>();
            }
        }
    }
}
