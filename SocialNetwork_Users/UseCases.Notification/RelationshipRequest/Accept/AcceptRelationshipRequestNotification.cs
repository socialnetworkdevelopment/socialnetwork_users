﻿using MediatR;

namespace UseCases.Notification.Accept
{
    public class AcceptRelationshipRequestNotification : INotification
    {
        public AcceptRelationshipRequestNotification(long requestId)
        {
            RequestId = requestId;
        }

        public long RequestId { get; set; }
    }
}
