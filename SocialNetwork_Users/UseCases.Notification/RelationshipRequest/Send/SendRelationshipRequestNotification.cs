﻿using MediatR;
using UseCases.Dto;

namespace UseCases.Notification.Send
{
    public class SendRelationshipRequestNotification : INotification
    {
        public SendRelationshipRequestNotification(long requestId)
        {
            RequestId = requestId;
        }

        public long RequestId { get; set; }
    }
}
