﻿using ChatNotification.Interfaces;
using DataAccess.Interfaces.Repositories;
using Entities;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using UseCases.Exceptions;
using UseCases.Validation;
using Utils;

namespace UseCases.Notification.Send
{
    public class SendRelationshipRequestClientNotificationHandler : INotificationHandler<SendRelationshipRequestNotification>
    {
        private readonly IServiceProvider _serviceProvider;

        public SendRelationshipRequestClientNotificationHandler(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task Handle(SendRelationshipRequestNotification notification, CancellationToken cancellationToken)
        {
            using (var scope = _serviceProvider.CreateAsyncScope())
            {
                IRelationshipRequestReadonlyRepository requestReadonlyRepository = scope.ServiceProvider.GetRequiredService<IRelationshipRequestReadonlyRepository>();
                RelationshipRequest relationshipRequest = await requestReadonlyRepository
                    .FirstOrDefaultAsync(request => request.Id == notification.RequestId,
                    includeProperties: new List<string> { nameof(RelationshipRequest.Sender),nameof(RelationshipRequest.Receiver) })
                    ?? throw new RelationshipRequestMissingException(notification.RequestId);

                await scope.ServiceProvider.GetRequiredService<IRelationshipRequestClientNotificator>()
                    .SendRelationshipRequest(relationshipRequest);
            }
        }
    }

    namespace Registration
    {
        public class CreatedUserNotificationHandlerRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                services.AddSingleton<INotificationHandler<SendRelationshipRequestNotification>, SendRelationshipRequestClientNotificationHandler>()
                    .Decorate<INotificationHandler<SendRelationshipRequestNotification>, BaseValidatorNotificationHandler<SendRelationshipRequestNotification>>();
            }
        }
    }
}
