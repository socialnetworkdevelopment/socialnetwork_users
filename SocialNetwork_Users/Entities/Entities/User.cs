﻿namespace Entities
{
    public class User : BaseEntity<long>
    {
        public string? FirstName { get; set; }

        public string? LastName { get; set; }

        public long? AvatarId { get; set; }

        public int? Age { get; set; }

        public string? City { get; set; } 

        public string? About { get; set; }

        public DateTime CreatedAt { get; set; }

        public ICollection<RelationshipRequest>? IncomingRelationshipRequests { get; set; }

        public ICollection<RelationshipRequest>? OutgoingRelationshipRequests { get; set; }

        public ICollection<Relationship>? Relationships1 { get; set; }

        public ICollection<Relationship>? Relationships2 { get; set; }
    }
}
