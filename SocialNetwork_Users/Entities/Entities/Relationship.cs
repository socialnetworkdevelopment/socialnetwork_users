﻿using Entities.Enums;

namespace Entities
{
    public class Relationship : BaseEntity<long>
    {
        public required User User1 { get; set; }

        public required User User2 { get; set; }

        public RelationshipType Type { get; set; }
    }
}
