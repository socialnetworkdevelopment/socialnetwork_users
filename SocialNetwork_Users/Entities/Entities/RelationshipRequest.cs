﻿using Entities.Enums;

namespace Entities
{
    public class RelationshipRequest : BaseEntity<long>
    {
        public required User Sender { get; set; }

        public required User Receiver { get; set; }
        
        public RelationshipType Type { get; set; }

        public DateTime CreateAt { get; set; }

        public bool Accepted { get; set; }

        public bool Rejected { get; set; }
    }
}
