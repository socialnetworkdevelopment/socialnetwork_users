﻿using ApplicationServices.Interfaces.CurrentUser;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace ApplicationServices.Implementations.CurrentUser
{
    public class CurrentUserProvider : ICurrentUserProvider<int>
    {
        public int GetCurrentUserId()
        {
            throw new NotImplementedException();
        }
    }

    namespace Registration
    {
        public class CurrentUserProviderRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
                => Check.NotNull(services).AddScoped<ICurrentUserProvider<int>, CurrentUserProvider>();
        }
    }
}
