﻿using ApplicationServices.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace ApplicationServices.Implementations
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetNow() => DateTime.UtcNow;
    }

    namespace Registration
    {
        public class DateTimeProviderRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddSingleton<IDateTimeProvider, DateTimeProvider>();
            }
        }
    }
}
