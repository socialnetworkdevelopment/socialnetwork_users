﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using Utils;

namespace ApplicationServices.Implementations.Logging
{
    public class LoggingBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        private readonly ILogger<LoggingBehaviour<TRequest, TResponse>> _logger;

        public LoggingBehaviour(ILogger<LoggingBehaviour<TRequest, TResponse>> logger)
            => _logger = logger;

        public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
        {
            var requestName = request.GetType().Name;
            var requestGuid = Guid.NewGuid();

            using (_logger.BeginScope("MediatRHandler:{@MediatorRequestGuid}", requestGuid))
            {
                _logger.LogInformation("[START] {@RequestName}", requestName);
                TResponse response;

                var stopwatch = Stopwatch.StartNew();
                try
                {
                    try
                    {
                        _logger.LogInformation("[PROPS] {@RequestName} {@Request}",
                            requestName, request);
                    }
                    catch (NotSupportedException)
                    {
                        _logger.LogInformation("[Serialization ERROR] {@RequestName} Could not serialize the request.",
                            requestName);
                    }

                    response = await next();
                }
                finally
                {
                    stopwatch.Stop();
                    _logger.LogInformation(
                        "[END] {@RequestName}; Execution time={@ExecutionTime}ms",
                        requestName, stopwatch.ElapsedMilliseconds);
                }

                return response;
            }
        }
    }

    namespace Registration
    {
        public class LoggingBehaviorRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingBehaviour<,>));
            }
        }
    }
}
