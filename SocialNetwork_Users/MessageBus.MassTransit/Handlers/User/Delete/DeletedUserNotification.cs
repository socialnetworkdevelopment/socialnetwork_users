﻿using MediatR;
using SocialNetwork.Contracts.User;

namespace MessageBus.MassTransit.Handlers.User.Delete
{
    public class DeletedUserNotification : INotification
    {
        public DeletedUserNotification(UserDeleted dto)
        {
            this.Dto = dto;
        }

        public UserDeleted Dto { get; private set; }
    }
}
