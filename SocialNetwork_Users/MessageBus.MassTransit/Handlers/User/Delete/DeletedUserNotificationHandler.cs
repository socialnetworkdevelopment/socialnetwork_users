﻿using MassTransit;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using UseCases.Validation;
using Utils;

namespace MessageBus.MassTransit.Handlers.User.Delete
{
    public class DeletedUserNotificationHandler : INotificationHandler<DeletedUserNotification>
    {
        private readonly IPublishEndpoint _publishEndpoint;

        public DeletedUserNotificationHandler(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }

        public async Task Handle(DeletedUserNotification notification, CancellationToken cancellationToken)
        {
            await _publishEndpoint.Publish(notification.Dto, cancellationToken);
        }
    }

    namespace Registration
    {
        public class DeletedUserNotificationHandlerRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                services.AddTransient<INotificationHandler<DeletedUserNotification>, DeletedUserNotificationHandler>()
                    .Decorate<INotificationHandler<DeletedUserNotification>, BaseValidatorNotificationHandler<DeletedUserNotification>>();
            }
        }
    }
}
