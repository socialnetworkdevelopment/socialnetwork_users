﻿using MassTransit;
using MediatR;
using MessageBus.MassTransit.Handlers.User.Create;
using Microsoft.Extensions.DependencyInjection;
using UseCases.Validation;
using Utils;

namespace MessageBus.MassTransit.Handlers.User.Update
{
    internal class UpdatedUserNotificationHandler : INotificationHandler<UpdatedUserNotification>
    {
        private readonly IPublishEndpoint _publishEndpoint;

        public UpdatedUserNotificationHandler(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }

        public async Task Handle(UpdatedUserNotification notification, CancellationToken cancellationToken)
        {
            await _publishEndpoint.Publish(notification.Dto, cancellationToken);
        }
    }

    namespace Registration
    {
        public class UpdatedUserNotificationHandlerRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                services.AddTransient<INotificationHandler<UpdatedUserNotification>, UpdatedUserNotificationHandler>()
                    .Decorate<INotificationHandler<UpdatedUserNotification>, BaseValidatorNotificationHandler<UpdatedUserNotification>>();
            }
        }
    }
}
