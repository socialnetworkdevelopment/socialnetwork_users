﻿using MediatR;
using SocialNetwork.Contracts.User;

namespace MessageBus.MassTransit.Handlers.User.Update
{
    public class UpdatedUserNotification : INotification
    {
        public UpdatedUserNotification(UserUpdated dto)
        {
            this.Dto = dto;
        }

        public UserUpdated Dto { get; private set; }
    }
}
