﻿using MediatR;
using SocialNetwork.Contracts.User;

namespace MessageBus.MassTransit.Handlers.User.UpdateAvatar
{
    public class UpdateUserAvatarNotification : INotification
    {
        public UpdateUserAvatarNotification(UserAvatarUpdated dto)
        {
            this.Dto = dto;
        }

        public UserAvatarUpdated Dto { get; private set; }
    }
}
