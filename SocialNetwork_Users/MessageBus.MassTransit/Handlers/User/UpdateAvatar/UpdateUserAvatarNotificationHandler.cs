﻿using MassTransit;
using MediatR;
using MessageBus.MassTransit.Handlers.User.Update;
using Microsoft.Extensions.DependencyInjection;
using UseCases.Validation;
using Utils;

namespace MessageBus.MassTransit.Handlers.User.UpdateAvatar
{
    internal class UpdateUserAvatarNotificationHandler : INotificationHandler<UpdateUserAvatarNotification>
    {
        private readonly IPublishEndpoint _publishEndpoint;

        public UpdateUserAvatarNotificationHandler(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }

        public async Task Handle(UpdateUserAvatarNotification notification, CancellationToken cancellationToken)
        {
            await _publishEndpoint.Publish(notification.Dto, cancellationToken);
        }
    }

    namespace Registration
    {
        public class UpdateUserAvatarNotificationHandlerRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                services.AddTransient<INotificationHandler<UpdateUserAvatarNotification>, UpdateUserAvatarNotificationHandler>()
                    .Decorate<INotificationHandler<UpdateUserAvatarNotification>, BaseValidatorNotificationHandler<UpdateUserAvatarNotification>>();
            }
        }
    }
}
