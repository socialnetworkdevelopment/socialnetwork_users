﻿using MediatR;
using SocialNetwork.Contracts.User;

namespace MessageBus.MassTransit.Handlers.User.Create
{
    public class CreatedUserNotification : INotification
    {
        public CreatedUserNotification(UserCreated dto)
        {
            this.Dto = dto;
        }

        public UserCreated Dto { get; private set; }
    }
}
