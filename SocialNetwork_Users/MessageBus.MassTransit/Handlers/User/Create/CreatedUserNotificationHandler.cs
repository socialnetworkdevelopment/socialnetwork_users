﻿using MassTransit;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using UseCases.Validation;
using Utils;

namespace MessageBus.MassTransit.Handlers.User.Create
{
    public class CreatedUserNotificationHandler : INotificationHandler<CreatedUserNotification>
    {
        private readonly IPublishEndpoint _publishEndpoint;

        public CreatedUserNotificationHandler(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }

        public async Task Handle(CreatedUserNotification notification, CancellationToken cancellationToken)
        {
            await _publishEndpoint.Publish(notification.Dto, cancellationToken);
        }
    }

    namespace Registration
    {
        public class CreatedUserNotificationHandlerRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                services.AddTransient<INotificationHandler<CreatedUserNotification>, CreatedUserNotificationHandler>()
                    .Decorate<INotificationHandler<CreatedUserNotification>, BaseValidatorNotificationHandler<CreatedUserNotification>>();
            }
        }
    }
}
