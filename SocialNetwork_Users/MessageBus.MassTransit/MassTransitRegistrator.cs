﻿using Microsoft.Extensions.DependencyInjection;
using SocialNetwork.Common.MassTransit;
using Utils;

namespace MessageBus.MassTransit
{
    public class MassTransitRegistrator : IServiceRegistrator
    {
        public void AddServices(IServiceCollection services)
        {
            Check.NotNull(services).AddMassTransitWithRabbitMq(typeof(AssemblyMarker).Assembly);
        }
    }
}
